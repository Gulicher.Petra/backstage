# [Backstage](https://backstage.io)

This is a Backstage App that has been scaffolded with [`npx @backstage/create-app@latest`](https://www.npmjs.com/package/@backstage/create-app).

To build & run a Backstage app, you will need to have [NodeJS Active LTS Release](https://nodejs.org/en/download/) installed (currently v20).

For the full list of system dependencies, see the [Backstage documentation](https://backstage.io/docs/getting-started/#prerequisites).

To start the app, run:

```sh
yarn install
yarn dev
```

## Authentication

Of the [built-in authentication providers](https://backstage.io/docs/auth/#built-in-authentication-providers), these could work for us:

Atlassian
Azure
GitLab

> The Backstage core-plugin-api package comes with a GitLab authentication provider that can authenticate users using [GitLab OAuth](https://backstage.io/docs/auth/gitlab/provider).


## Integrations

### GitLab

Backstage backend plugins enable authentication and discovery of Locations and Entities from GitLab.

Documentation on how it works can be found here: https://backstage.io/docs/integrations/gitlab/locations

## TechDocs

Backstage can embed documentation found in GitLab repositories.

Read more about how it works: https://backstage.io/docs/features/techdocs/

## Demo Documentation

Progress on this demo is tracked on Confluence: https://confluence.abc-dev.net.au/pages/viewpage.action?pageId=310591915
